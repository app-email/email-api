import { Test, TestingModule } from '@nestjs/testing';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Types } from 'mongoose';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { LoginAuthDto } from './dto/login-auth.dto';
import { User } from '../user/schemas/user.schema';
import { UserRepository } from '../user/user.repository';

const userMock: User = {
  _id: new Types.ObjectId('63df42ec77e0abcce75f17d8'),
  username: 'user@example.com',
  password: '$2b$10$CctoYNhzqtjYMsvjWj10XuAJr9l7r2FEnjovfmxkFMAEAEM/aTUTC',
  createdAt: new Date(),
  updatedAt: new Date(),
};

class UserRepositoryMock {
  static async findByUsername(id: string): Promise<User> {
    return userMock;
  }
}
class JwtServiceMock {
  static sign(payload): string {
    const token = 'Token JWT';
    return token;
  }
}

describe('AuthService', () => {
  let service: AuthService;
  const JWT_SECRET = 'secret';
  const JWT_LIFETIME = '3000';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.register({
          secret: `${JWT_SECRET}`,
          signOptions: { expiresIn: `${JWT_LIFETIME}s` },
        }),
      ],
      controllers: [AuthController],
      providers: [
        {
          provide: UserRepository,
          useValue: UserRepositoryMock,
        },
        {
          provide: JwtService,
          useValue: JwtServiceMock,
        },
        AuthService,
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('login | SUCCESS', async () => {
    const loginAuthDto: LoginAuthDto = {
      username: 'user@example.com',
      password: '123456',
    };
    const response: any = await service.login(loginAuthDto);
    expect(response).toBeDefined();
  });
});
