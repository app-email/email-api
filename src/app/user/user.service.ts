import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { APP_SALT } from 'src/config/app/app.constants';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './schemas/user.schema';
import { UserRepository } from './user.repository';

@Injectable()
export class UserService {
  constructor(private userRepository: UserRepository) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const passwordHased: string = await bcrypt.hash(
      createUserDto.password,
      APP_SALT,
    );
    const user: User = await this.userRepository.create({
      ...createUserDto,
      password: passwordHased,
    });
    delete user.password;
    return user;
  }

  async findAll(): Promise<User[]> {
    const users: User[] = await this.userRepository.findAll();
    return users;
  }

  async findOne(id: string): Promise<User> {
    const user: User = await this.userRepository.findOne(id);
    return user;
  }

  async remove(id: string): Promise<User> {
    const user: User = await this.userRepository.remove(id);
    return user;
  }
}
