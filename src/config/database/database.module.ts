import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { DATABASE_URI } from './database.constants';

@Module({
  imports: [MongooseModule.forRoot(`${DATABASE_URI}`)],
})
export class DatabaseModule {}
