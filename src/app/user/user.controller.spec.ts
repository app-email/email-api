import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { Types } from 'mongoose';
import { User } from './schemas/user.schema';
import { UserRepository } from './user.repository';

const userMock: User = {
  _id: new Types.ObjectId('63df42ec77e0abcce75f17d8'),
  username: 'user@example.com',
  password: '$2b$10$CctoYNhzqtjYMsvjWj10XuAJr9l7r2FEnjovfmxkFMAEAEM/aTUTC',
  createdAt: new Date(),
  updatedAt: new Date(),
};

const usersMock: User[] = [
  { ...userMock, _id: new Types.ObjectId('63df42ec77e0abcce75f17d8') },
  { ...userMock, _id: new Types.ObjectId('63df858411e48b4241bf20d0') },
  { ...userMock, _id: new Types.ObjectId('63df9b40aadde47415657c74') },
];

class UserRepositoryMock {
  static async findAll(): Promise<User[]> {
    return usersMock;
  }
  static async findOne(id: string): Promise<User> {
    return userMock;
  }
}

describe('UserController', () => {
  let controller: UserController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        {
          provide: UserRepository,
          useValue: UserRepositoryMock,
        },
        UserService,
      ],
    }).compile();

    controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
