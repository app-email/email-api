import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { Model } from 'mongoose';
import { User, UserDocument } from 'src/app/user/schemas/user.schema';

@ValidatorConstraint({ name: 'CheckUserExists', async: true })
@Injectable()
export class CheckUserExists implements ValidatorConstraintInterface {
  constructor(
    @InjectModel(User.name)
    private userModel: Model<UserDocument>,
  ) {}

  async validate(value: string): Promise<boolean> {
    const user: User = await this.userModel.findById(value);
    return !!user;
  }
}
