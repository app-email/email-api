import { Injectable } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { User } from 'src/app/user/schemas/user.schema';
import { UserRepository } from 'src/app/user/user.repository';

@ValidatorConstraint({ name: 'CheckMessageExists', async: true })
@Injectable()
export class CheckMessageExists implements ValidatorConstraintInterface {
  constructor(private userRepository: UserRepository) {}

  async validate(value: string): Promise<boolean> {
    const user: User = await this.userRepository.findOne(value);
    return !!user;
  }
}
