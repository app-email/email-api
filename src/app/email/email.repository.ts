import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  Message,
  MessageDocument,
} from 'src/app/message/schemas/message.schema';
import { Category, Email, EmailDocument } from './schemas/email.schema';
import { CreateEmailDto } from './dto/create-email.dto';
import { UpdateEmailDto } from './dto/update-email.dto';

@Injectable()
export class EmailRepository {
  constructor(
    @InjectModel(Email.name)
    private emailModel: Model<EmailDocument>,
    @InjectModel(Message.name)
    private messageModel: Model<MessageDocument>,
  ) {}

  async create(createEmailDto: CreateEmailDto): Promise<Email> {
    const email = new this.emailModel(createEmailDto);
    await email.save();
    return email;
  }

  async findAll(): Promise<Email[]> {
    const emails: Email[] = await this.emailModel
      .find()
      .populate('message', '', this.messageModel)
      .sort({ updatedAt: -1 })
      .exec();
    return emails;
  }

  async findAllByUser(id: string, category: Category): Promise<Email[]> {
    const emails: Email[] = await this.emailModel
      .find({ category })
      .populate('message', '', this.messageModel)
      .sort({ updatedAt: -1 })
      .exec();
    return emails;
  }

  async findOne(id: string): Promise<Email> {
    const email: Email = await this.emailModel.findById(id).exec();
    return email;
  }

  async update(id: string, updateEmailDto: UpdateEmailDto): Promise<Email> {
    await this.emailModel.findByIdAndUpdate(id, updateEmailDto).exec();
    const email: Email = await this.emailModel.findById(id).exec();
    return email;
  }
}
