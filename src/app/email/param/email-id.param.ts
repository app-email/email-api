import { IsString, Validate } from 'class-validator';
import { CheckEmailExists } from 'src/app/email/validators/check-email-exists.validator';

export class EmailIdParam {
  @IsString()
  @Validate(CheckEmailExists, { message: 'Email does not exists' })
  id: string;
}
