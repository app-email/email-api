import { ENVIRONMENT } from '../config.constants';

export const DATABASE_URI: string = ENVIRONMENT.DATABASE_URI;
