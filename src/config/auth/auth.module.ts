import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JWT_SECRET, JWT_LIFETIME } from './auth.constants';

@Module({
  imports: [
    PassportModule,
    {
      ...JwtModule.register({
        secret: `${JWT_SECRET}`,
        signOptions: { expiresIn: `${JWT_LIFETIME}s` },
      }),
      global: true,
    },
  ],
  providers: [],
  exports: [JwtModule],
})
export class AuthModule {}
