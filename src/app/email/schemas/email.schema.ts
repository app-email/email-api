import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Types, now } from 'mongoose';
import { Message } from 'src/app/message/schemas/message.schema';

export enum Category {
  SENT = 'SENT',
  INBOX = 'INBOX',
  TRASH = 'TRASH',
}

export enum ViewStatus {
  UNREAD = 'UNREAD',
  READ = 'READ',
}

export enum EntityType {
  SENDER = 'SENDER',
  RECEIVER = 'RECEIVER',
}

export type EmailDocument = HydratedDocument<Email>;

@Schema({ timestamps: true })
export class Email {
  _id: Types.ObjectId;

  @Prop({ required: true })
  user: Types.ObjectId;

  @Prop({ required: true, type: Types.ObjectId, ref: 'message' })
  message: Message;

  @Prop({
    type: String,
    required: true,
    enum: [Category.SENT, Category.INBOX, Category.TRASH],
  })
  category: Category;

  @Prop({
    type: String,
    required: true,
    enum: [ViewStatus.UNREAD, ViewStatus.READ],
  })
  viewStatus: ViewStatus;

  @Prop({
    type: String,
    required: true,
    enum: [EntityType.SENDER, EntityType.RECEIVER],
  })
  entityType: EntityType;

  @Prop({ default: now() })
  createdAt: Date;

  @Prop({ default: now() })
  updatedAt: Date;
}

export const EmailSchema = SchemaFactory.createForClass(Email);
