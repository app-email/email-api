import { IsNotEmpty, IsString, Validate } from 'class-validator';
import { CheckEmailExists } from '../validators/check-email-exists.validator';

export class RestoreMessageDto {
  @IsNotEmpty({ message: 'Email is required' })
  @IsString()
  @Validate(CheckEmailExists, { message: 'Email does not exists' })
  message: string;
}
