import { IsNotEmpty, IsString, MaxLength, Validate } from 'class-validator';
import { CheckUsernameExists } from 'src/app/user/validators/check-username-exists.validator';

export class CreateMessageDto {
  @IsNotEmpty({ message: 'Email from is required' })
  @IsString()
  @Validate(CheckUsernameExists, { message: 'Email from does not exists' })
  from: string;

  @IsNotEmpty({ message: 'Email to is required' })
  @IsString()
  @Validate(CheckUsernameExists, { message: 'Email to does not exists' })
  to: string;

  @IsNotEmpty({ message: 'Subject is required' })
  @IsString()
  subject: string;

  @IsNotEmpty({ message: 'Body message is required' })
  @MaxLength(2000, {
    message:
      'Body message is too long. Maximal length is $constraint1 characters, but actual is $value',
  })
  @IsString()
  body: string;
}
