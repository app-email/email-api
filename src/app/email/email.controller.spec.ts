import { Test, TestingModule } from '@nestjs/testing';
import { EmailController } from './email.controller';
import { EmailService } from './email.service';
import { EmailRepository } from './email.repository';
import { MessageRepository } from '../message/message.repository';
import { UserRepository } from '../user/user.repository';
import { Email } from './schemas/email.schema';
import { User } from '../user/schemas/user.schema';
import { Message } from '../message/schemas/message.schema';

class UserRepositoryMock {
  static async findAll(): Promise<User[]> {
    return [];
  }
  static async findOne(id: string): Promise<User> {
    return null;
  }
}

class EmailRepositoryMock {
  static async findAll(): Promise<Email[]> {
    return [];
  }
  static async findOne(id: string): Promise<Email> {
    return null;
  }
}

class MessageRepositoryMock {
  static async findAll(): Promise<Message[]> {
    return [];
  }
  static async findOne(id: string): Promise<Message> {
    return null;
  }
}

describe('EmailController', () => {
  let controller: EmailController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EmailController],
      providers: [
        {
          provide: EmailRepository,
          useValue: EmailRepositoryMock,
        },
        {
          provide: MessageRepository,
          useValue: MessageRepositoryMock,
        },
        {
          provide: UserRepository,
          useValue: UserRepositoryMock,
        },
        EmailService,
      ],
    }).compile();

    controller = module.get<EmailController>(EmailController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
