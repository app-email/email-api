import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule as AuthConfigModule } from 'src/config/auth/auth.module';
import { JwtStrategy } from 'src/config/auth/jwt.strategy';
import { User, UserSchema } from 'src/app/user/schemas/user.schema';
import { UserRepository } from 'src/app/user/user.repository';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { CheckUsernameExists } from 'src/app/user/validators/check-username-exists.validator';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    AuthConfigModule,
  ],
  providers: [AuthService, UserRepository, CheckUsernameExists, JwtStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
