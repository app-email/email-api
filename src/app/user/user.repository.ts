import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { User, UserDocument } from './schemas/user.schema';

@Injectable()
export class UserRepository {
  constructor(
    @InjectModel(User.name)
    private userModel: Model<UserDocument>,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const user = new this.userModel({ ...createUserDto });
    await user.save();
    delete user.password;
    return user;
  }

  async findAll(): Promise<User[]> {
    const users: User[] = await this.userModel
      .find()
      .sort({ updatedAt: -1 })
      .exec();
    users.forEach((item) => delete item.password);
    return users;
  }

  async findByUsername(username: string): Promise<User> {
    const user: User = await this.userModel.findOne({ username }).exec();
    delete user.password;
    return user;
  }

  async findOne(id: string): Promise<User> {
    const user: User = await this.userModel.findById(id).exec();
    delete user.password;
    return user;
  }

  async remove(id: string): Promise<User> {
    const user: User = await this.userModel.findByIdAndRemove(id).exec();
    delete user.password;
    return user;
  }
}
