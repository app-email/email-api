import { Test, TestingModule } from '@nestjs/testing';
import { MessageService } from './message.service';
import { Message } from './schemas/message.schema';
import { MessageRepository } from './message.repository';
import { Types } from 'mongoose';

const messageMock: Message = {
  _id: new Types.ObjectId('63df42ec77e0abcce75f17d8'),
  from: 'email1@example.com',
  to: 'email1@example.com',
  subject: 'Subject Example',
  body: 'Body Example',
  createdAt: new Date(),
  updatedAt: new Date(),
};

const messagesMock: Message[] = [
  { ...messageMock, _id: new Types.ObjectId('63df42ec77e0abcce75f17d8') },
  { ...messageMock, _id: new Types.ObjectId('63df858411e48b4241bf20d0') },
  { ...messageMock, _id: new Types.ObjectId('63df9b40aadde47415657c74') },
];

class MessageRepositoryMock {
  static async findAll(): Promise<Message[]> {
    return messagesMock;
  }
  static async findOne(id: string): Promise<Message> {
    return messageMock;
  }
}

describe('MessageService', () => {
  let service: MessageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [
        {
          provide: MessageRepository,
          useValue: MessageRepositoryMock,
        },
        MessageService,
      ],
    }).compile();

    service = module.get<MessageService>(MessageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('findAll | SUCCESS', async () => {
    const messages: Message[] = await service.findAll();
    expect(messagesMock).toBe(messages);
  });

  it('findOne | SUCCESS', async () => {
    const id = '63df42ec77e0abcce75f17d8';
    const message: Message = await service.findOne(id);
    expect(messageMock).toBe(message);
  });
});
