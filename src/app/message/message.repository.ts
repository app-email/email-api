import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Message, MessageDocument } from './schemas/message.schema';
import { CreateMessageDto } from './dto/create-message.dto';

@Injectable()
export class MessageRepository {
  constructor(
    @InjectModel(Message.name)
    private messageModel: Model<MessageDocument>,
  ) {}

  async create(createMessageDto: CreateMessageDto): Promise<Message> {
    const message = new this.messageModel(createMessageDto);
    await message.save();
    return message;
  }

  async findAll(): Promise<Message[]> {
    const messages: Message[] = await this.messageModel
      .find()
      .sort({ updatedAt: -1 })
      .exec();
    return messages;
  }

  async findOne(id: string): Promise<Message> {
    const message: Message = await this.messageModel.findById(id).exec();
    return message;
  }
}
