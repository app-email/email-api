import { Injectable } from '@nestjs/common';
import { MessageRepository } from 'src/app/message/message.repository';
import { Message } from 'src/app/message/schemas/message.schema';
import { UserRepository } from 'src/app/user/user.repository';
import {
  Category,
  Email,
  EntityType,
  ViewStatus,
} from './schemas/email.schema';
import { SendEmailDto } from './dto/send-email.dto';
import { EmailRepository } from './email.repository';

@Injectable()
export class EmailService {
  constructor(
    private emailRepository: EmailRepository,
    private userRepository: UserRepository,
    private messageRepository: MessageRepository,
  ) {}

  async sendEmail(sendEmailDto: SendEmailDto): Promise<Email> {
    const senderUserPromise = this.userRepository.findByUsername(
      sendEmailDto.from,
    );
    const receiverUserPromise = this.userRepository.findByUsername(
      sendEmailDto.to,
    );
    const [senderUser, receiverUser] = await Promise.all([
      senderUserPromise,
      receiverUserPromise,
    ]);

    const message: Message = await this.messageRepository.create({
      from: sendEmailDto.from,
      to: sendEmailDto.to,
      subject: sendEmailDto.subject,
      body: sendEmailDto.body,
    });

    const sentEmailPromise = this.emailRepository.create({
      message: message._id,
      user: senderUser._id,
      category: Category.SENT,
      viewStatus: ViewStatus.READ,
      entityType: EntityType.SENDER,
    });
    const receivedEmailPromise = this.emailRepository.create({
      message: message._id,
      user: receiverUser._id,
      category: Category.INBOX,
      viewStatus: ViewStatus.UNREAD,
      entityType: EntityType.RECEIVER,
    });

    const [sentEmail] = await Promise.all([
      sentEmailPromise,
      receivedEmailPromise,
    ]);
    return sentEmail;
  }

  async findAll(): Promise<Email[]> {
    const emails: Email[] = await this.emailRepository.findAll();
    return emails;
  }

  async findFromInbox(id: string): Promise<Email[]> {
    const emails: Email[] = await this.emailRepository.findAllByUser(
      id,
      Category.INBOX,
    );
    return emails;
  }

  async findFromSent(id: string): Promise<Email[]> {
    const emails: Email[] = await this.emailRepository.findAllByUser(
      id,
      Category.SENT,
    );
    return emails;
  }

  async findFromThrash(id: string): Promise<Email[]> {
    const emails: Email[] = await this.emailRepository.findAllByUser(
      id,
      Category.TRASH,
    );
    return emails;
  }

  async findOne(id: string): Promise<Email> {
    const email: Email = await this.emailRepository.findOne(id);
    return email;
  }

  async markAsRead(id: string): Promise<Email> {
    const email: Email = await this.emailRepository.update(id, {
      viewStatus: ViewStatus.READ,
    });
    return email;
  }

  async markAsUnread(id: string): Promise<Email> {
    const email: Email = await this.emailRepository.update(id, {
      viewStatus: ViewStatus.UNREAD,
    });
    return email;
  }

  async moveToTrash(id: string): Promise<Email> {
    const email: Email = await this.emailRepository.update(id, {
      category: Category.TRASH,
    });
    return email;
  }

  async restore(id: string): Promise<Email> {
    const deletedEmail: Email = await this.emailRepository.findOne(id);
    const category: Category =
      deletedEmail.entityType === EntityType.SENDER
        ? Category.SENT
        : Category.INBOX;

    const restoredMail: Email = await this.emailRepository.update(id, {
      category,
    });
    return restoredMail;
  }
}
