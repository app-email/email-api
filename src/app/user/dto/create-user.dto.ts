import { IsNotEmpty, IsString, MaxLength, Validate } from 'class-validator';
import { IsUsernameUnique } from '../validators/is-username-unique.validator';

export class CreateUserDto {
  @IsNotEmpty({ message: 'Email username is required' })
  @IsString()
  @MaxLength(50, {
    message:
      'Email username is too long. Maximal length is $constraint1 characters, but actual is $value',
  })
  @Validate(IsUsernameUnique, { message: 'Email exists' })
  username: string;

  @IsNotEmpty({ message: 'Password is required' })
  @IsString()
  @MaxLength(50, {
    message:
      'Password is too long. Maximal length is $constraint1 characters, but actual is $value',
  })
  password: string;
}
