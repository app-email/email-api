import { Test, TestingModule } from '@nestjs/testing';
import { JwtService } from '@nestjs/jwt';
import { Types } from 'mongoose';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { User } from '../user/schemas/user.schema';
import { UserRepository } from '../user/user.repository';

const userMock: User = {
  _id: new Types.ObjectId('63df42ec77e0abcce75f17d8'),
  username: 'user@example.com',
  password: '$2b$10$CctoYNhzqtjYMsvjWj10XuAJr9l7r2FEnjovfmxkFMAEAEM/aTUTC',
  createdAt: new Date(),
  updatedAt: new Date(),
};

class UserRepositoryMock {
  static async findByUsername(id: string): Promise<User> {
    return userMock;
  }
}

describe('AuthController', () => {
  let controller: AuthController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: UserRepository,
          useValue: UserRepositoryMock,
        },
        AuthService,
        JwtService,
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
