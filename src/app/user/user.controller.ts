import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './schemas/user.schema';

@Controller('user/users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  async create(@Res() res, @Body() createUserDto: CreateUserDto) {
    const user: User = await this.userService.create(createUserDto);
    delete user.password;
    return res.status(HttpStatus.CREATED).json(user);
  }

  @Get()
  async findAll(@Res() res) {
    const users: User[] = await await this.userService.findAll();
    users.forEach((item) => delete item.password);
    const response = res.status(HttpStatus.OK).json(users);
    return response;
  }

  @Get(':id')
  async findOne(@Res() res, @Param('id') id: string) {
    const user: User = await this.userService.findOne(id);
    const response = res.status(HttpStatus.OK).json(user);
    return response;
  }

  @Delete(':id')
  async remove(@Res() res, @Param('id') id: string): Promise<any> {
    await this.userService.remove(id);
    const response = res.status(HttpStatus.NO_CONTENT);
    return response;
  }
}
