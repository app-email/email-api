import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { Email, EmailDocument } from '../schemas/email.schema';

@ValidatorConstraint({ name: 'CheckEmailExists', async: true })
@Injectable()
export class CheckEmailExists implements ValidatorConstraintInterface {
  constructor(
    @InjectModel(Email.name)
    private emailModel: Model<EmailDocument>,
  ) {}

  async validate(value: string): Promise<boolean> {
    try {
      const email: Email = await this.emailModel.findById(value);
      return !!email;
    } catch (error) {
      return false;
    }
  }
}
