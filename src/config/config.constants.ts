import * as fs from 'fs';
import { parse } from 'dotenv';

const ENVIRONMENT_PATH = `.env`;
const exists = fs.existsSync(ENVIRONMENT_PATH);

try {
  if (!exists) {
    throw new Error('.env file does not exists');
  }
} catch (error) {
  console.error(error);
  process.exit(0);
}

export const ENVIRONMENT = parse(fs.readFileSync(ENVIRONMENT_PATH));
