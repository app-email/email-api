import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from 'src/app/user/schemas/user.schema';
import { Message, MessageSchema } from 'src/app/message/schemas/message.schema';
import { MessageRepository } from 'src/app/message/message.repository';
import { MessageModule } from 'src/app/message/message.module';
import { UserRepository } from 'src/app/user/user.repository';
import { CheckUserExists } from 'src/app/user/validators/check-user-exists.validator';
import { CheckEmailExists } from './validators/check-email-exists.validator';
import { EmailController } from './email.controller';
import { EmailService } from './email.service';
import { Email, EmailSchema } from './schemas/email.schema';
import { EmailRepository } from './email.repository';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Email.name, schema: EmailSchema },
      { name: Message.name, schema: MessageSchema },
      { name: User.name, schema: UserSchema },
    ]),
    MessageModule,
  ],
  controllers: [EmailController],
  providers: [
    EmailRepository,
    EmailService,
    MessageRepository,
    UserRepository,
    CheckUserExists,
    CheckEmailExists,
  ],
  exports: [EmailRepository],
})
export class EmailModule {}
