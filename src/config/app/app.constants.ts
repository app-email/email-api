import { ENVIRONMENT } from '../config.constants';

export const APP_NAME: string = ENVIRONMENT.APP_NAME;
export const APP_PORT: string = ENVIRONMENT.APP_PORT;
export const APP_SALT = Number(ENVIRONMENT.APP_SALT);
