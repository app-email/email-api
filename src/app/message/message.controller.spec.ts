import { Test, TestingModule } from '@nestjs/testing';
import { MessageController } from './message.controller';
import { MessageService } from './message.service';
import { Types } from 'mongoose';
import { Message } from './schemas/message.schema';
import { MessageRepository } from './message.repository';

export const messageMock: Message = {
  _id: new Types.ObjectId('63df42ec77e0abcce75f17d8'),
  from: 'email1@example.com',
  to: 'email1@example.com',
  subject: 'Subject Example',
  body: 'Body Example',
  createdAt: new Date(),
  updatedAt: new Date(),
};

export const messagesMock: Message[] = [
  { ...messageMock, _id: new Types.ObjectId('63df42ec77e0abcce75f17d8') },
  { ...messageMock, _id: new Types.ObjectId('63df858411e48b4241bf20d0') },
  { ...messageMock, _id: new Types.ObjectId('63df9b40aadde47415657c74') },
];

export class MessageRepositoryMock {
  static async findAll(): Promise<Message[]> {
    return messagesMock;
  }
  static async findOne(id: string): Promise<Message> {
    return messageMock;
  }
}

describe('MessageController', () => {
  let controller: MessageController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MessageController],
      providers: [
        {
          provide: MessageRepository,
          useValue: MessageRepositoryMock,
        },
        MessageService,
      ],
    }).compile();

    controller = module.get<MessageController>(MessageController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
