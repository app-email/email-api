import { Types } from 'mongoose';
import { IsEnum, IsNotEmpty, IsString, Validate } from 'class-validator';
import { CheckUserExists } from 'src/app/user/validators/check-user-exists.validator';
import { CheckMessageExists } from 'src/app/message/validators/check-message-exists.validator';
import { Category, ViewStatus, EntityType } from '../schemas/email.schema';

export class CreateEmailDto {
  @IsNotEmpty({ message: 'User is required' })
  @IsString()
  @Validate(CheckUserExists, { message: 'User does not exists' })
  user: Types.ObjectId;

  @IsNotEmpty({ message: 'Message is required' })
  @IsString()
  @Validate(CheckMessageExists, { message: 'Message does not exists' })
  message: Types.ObjectId;

  @IsNotEmpty({ message: 'Category is required' })
  @IsString()
  @IsEnum(Category)
  category: Category;

  @IsNotEmpty({ message: 'View status is required' })
  @IsString()
  @IsEnum(ViewStatus)
  viewStatus: ViewStatus;

  @IsNotEmpty({ message: 'Message type is required' })
  @IsString()
  @IsEnum(EntityType)
  entityType: EntityType;
}
