import { IsNotEmpty, IsString, MaxLength, Validate } from 'class-validator';
import { CheckUsernameExists } from 'src/app/user/validators/check-username-exists.validator';

export class LoginAuthDto {
  @IsNotEmpty({ message: 'Email is required' })
  @IsString()
  @MaxLength(50, {
    message:
      'Email username is too long. Maximal length is $constraint1 characters, but actual is $value',
  })
  @Validate(CheckUsernameExists, { message: 'Email username doesnt exists' })
  username: string;

  @IsNotEmpty({ message: 'Password is required' })
  @IsString()
  @MaxLength(50, {
    message:
      'Password is too long. Maximal length is $constraint1 characters, but actual is $value',
  })
  password: string;
}
