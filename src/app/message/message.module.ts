import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from 'src/app/user/schemas/user.schema';
import { Email, EmailSchema } from 'src/app/email/schemas/email.schema';
import { CheckUsernameExists } from 'src/app/user/validators/check-username-exists.validator';
import { MessageService } from './message.service';
import { MessageController } from './message.controller';
import { Message, MessageSchema } from './schemas/message.schema';
import { MessageRepository } from './message.repository';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Message.name, schema: MessageSchema },
      { name: Email.name, schema: EmailSchema },
      { name: User.name, schema: UserSchema },
    ]),
  ],
  controllers: [MessageController],
  providers: [MessageRepository, MessageService, CheckUsernameExists],
  exports: [MessageRepository],
})
export class MessageModule {}
