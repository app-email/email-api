import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Post,
  Request,
  Res,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/config/auth/jwt-auth.guard';
import { JwtPayload } from 'src/config/auth/jwt-payload.model';
import { LoginAuthDto } from './dto/login-auth.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async login(@Res() res, @Body() loginAuthDto: LoginAuthDto): Promise<any> {
    const jwt: any = await this.authService.login(loginAuthDto);
    return res.status(HttpStatus.OK).json(jwt);
  }

  @UseGuards(JwtAuthGuard)
  @Get('authenticate')
  async authenticate(@Request() req, @Res() res): Promise<any> {
    const user: JwtPayload = req.user;
    return res.status(HttpStatus.OK).json(user);
  }
}
