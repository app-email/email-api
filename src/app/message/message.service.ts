import { Injectable } from '@nestjs/common';
import { Message } from './schemas/message.schema';
import { MessageRepository } from './message.repository';

@Injectable()
export class MessageService {
  constructor(private messageRepository: MessageRepository) {}

  async findAll(): Promise<Message[]> {
    const messages: Message[] = await this.messageRepository.findAll();
    return messages;
  }

  async findOne(id: string): Promise<Message> {
    const message: Message = await this.messageRepository.findOne(id);
    return message;
  }
}
