import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';
import { LoginAuthDto } from './dto/login-auth.dto';
import { User, UserDocument } from 'src/app/user/schemas/user.schema';
import { UserRepository } from '../user/user.repository';

@Injectable()
export class AuthService {
  constructor(
    private userRepository: UserRepository,
    private jwtService: JwtService,
  ) {}

  async login(loginAuthDto: LoginAuthDto): Promise<any> {
    const username: string = loginAuthDto.username;
    const user: User = await this.userRepository.findByUsername(username);

    const isValid: boolean = await bcrypt.compare(
      loginAuthDto.password,
      user.password,
    );
    if (!isValid) {
      throw new BadRequestException('Credentials error', {
        cause: new Error('Invalid password'),
        description: 'Invalid password',
      });
    }

    const payload = { id: user._id, username };
    const response = { accessToken: this.jwtService.sign(payload) };
    return response;
  }
}
