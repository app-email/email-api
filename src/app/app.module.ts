import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { EmailModule } from './email/email.module';
import { MessageModule } from './message/message.module';

@Module({
  imports: [UserModule, AuthModule, EmailModule, MessageModule],
})
export class AppModule {}
