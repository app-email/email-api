import { Test, TestingModule } from '@nestjs/testing';
import { EmailService } from './email.service';
import { Email } from './schemas/email.schema';
import { EmailRepository } from './email.repository';
import { EmailController } from './email.controller';
import { Message } from '../message/schemas/message.schema';
import { User } from '../user/schemas/user.schema';
import { MessageRepository } from '../message/message.repository';
import { UserRepository } from '../user/user.repository';

class UserRepositoryMock {
  static async findAll(): Promise<User[]> {
    return [];
  }
  static async findOne(id: string): Promise<User> {
    return null;
  }
}

class EmailRepositoryMock {
  static async findAll(): Promise<Email[]> {
    return [];
  }
  static async findOne(id: string): Promise<Email> {
    return null;
  }
}

class MessageRepositoryMock {
  static async findAll(): Promise<Message[]> {
    return [];
  }
  static async findOne(id: string): Promise<Message> {
    return null;
  }
}

describe('EmailService', () => {
  let service: EmailService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EmailController],
      providers: [
        {
          provide: EmailRepository,
          useValue: EmailRepositoryMock,
        },
        {
          provide: MessageRepository,
          useValue: MessageRepositoryMock,
        },
        {
          provide: UserRepository,
          useValue: UserRepositoryMock,
        },
        EmailService,
      ],
    }).compile();

    service = module.get<EmailService>(EmailService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
