import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Request,
  Res,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/config/auth/jwt-auth.guard';
import { JwtPayload } from 'src/config/auth/jwt-payload.model';
import { EmailService } from './email.service';
import { Email } from './schemas/email.schema';
import { SendEmailDto } from './dto/send-email.dto';
import { EmailIdParam } from './param/email-id.param';

@Controller('email')
export class EmailController {
  constructor(private readonly emailService: EmailService) {}

  @UseGuards(JwtAuthGuard)
  @Post('emails')
  async sendEmail(
    @Request() req,
    @Res() res,
    @Body() sendEmailDto: SendEmailDto,
  ): Promise<any> {
    const user: JwtPayload = req.user;
    const email: any = await this.emailService.sendEmail({
      ...sendEmailDto,
      from: user.username,
    });
    return res.status(HttpStatus.OK).json(email);
  }

  @UseGuards(JwtAuthGuard)
  @Get('emails')
  async findAll(@Res() res) {
    const emails: Email[] = await await this.emailService.findAll();
    const response = res.status(HttpStatus.OK).json(emails);
    return response;
  }

  @UseGuards(JwtAuthGuard)
  @Get('inbox-emails')
  async findInbox(@Request() req, @Res() res) {
    const { id }: JwtPayload = req.user;
    const emails: Email[] = await await this.emailService.findFromInbox(id);
    const response = res.status(HttpStatus.OK).json(emails);
    return response;
  }

  @UseGuards(JwtAuthGuard)
  @Get('sent-mails')
  async findSent(@Request() req, @Res() res) {
    const { id }: JwtPayload = req.user;
    const emails: Email[] = await await this.emailService.findFromSent(id);
    const response = res.status(HttpStatus.OK).json(emails);
    return response;
  }

  @UseGuards(JwtAuthGuard)
  @Get('trash-mails')
  async findTrash(@Request() req, @Res() res) {
    const { id }: JwtPayload = req.user;
    const emails: Email[] = await await this.emailService.findFromThrash(id);
    const response = res.status(HttpStatus.OK).json(emails);
    return response;
  }

  @UseGuards(JwtAuthGuard)
  @Get('emails/:id')
  async findOne(@Res() res, @Param() { id }: EmailIdParam) {
    const email: Email = await this.emailService.findOne(id);
    const response = res.status(HttpStatus.OK).json(email);
    return response;
  }

  @UseGuards(JwtAuthGuard)
  @Post('emails/:id/mark-as-read')
  async markAsRead(@Res() res, @Param() { id }: EmailIdParam) {
    const email: Email = await this.emailService.markAsRead(id);
    const response = res.status(HttpStatus.OK).json(email);
    return response;
  }

  @UseGuards(JwtAuthGuard)
  @Post('emails/:id/mark-as-unread')
  async markAsUnread(@Res() res, @Param() { id }: EmailIdParam) {
    const email: Email = await this.emailService.markAsUnread(id);
    const response = res.status(HttpStatus.OK).json(email);
    return response;
  }

  @UseGuards(JwtAuthGuard)
  @Post('emails/:id/move-to-trash')
  async moveToTrash(@Res() res, @Param() { id }: EmailIdParam) {
    const email: Email = await this.emailService.moveToTrash(id);
    const response = res.status(HttpStatus.OK).json(email);
    return response;
  }

  @UseGuards(JwtAuthGuard)
  @Post('emails/:id/restore')
  async restore(@Res() res, @Param() { id }: EmailIdParam) {
    const email: Email = await this.emailService.restore(id);
    const response = res.status(HttpStatus.OK).json(email);
    return response;
  }
}
