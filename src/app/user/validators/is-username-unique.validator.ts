import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { User, UserDocument } from 'src/app/user/schemas/user.schema';

@ValidatorConstraint({ name: 'IsUsernameUnique', async: true })
@Injectable()
export class IsUsernameUnique implements ValidatorConstraintInterface {
  constructor(
    @InjectModel(User.name)
    private userModel: Model<UserDocument>,
  ) {}

  async validate(value: string): Promise<boolean> {
    const user: User = await this.userModel.findOne({ username: value });
    return !user;
  }
}
