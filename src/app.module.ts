import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { AppModule as CoreModule } from './app/app.module';

@Module({
  imports: [ConfigModule, CoreModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
