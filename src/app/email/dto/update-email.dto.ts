import { IsEnum, IsOptional, IsString } from 'class-validator';
import { Category, ViewStatus, EntityType } from '../schemas/email.schema';

export class UpdateEmailDto {
  @IsOptional()
  @IsString()
  @IsEnum(Category)
  category?: Category;

  @IsOptional()
  @IsString()
  @IsEnum(ViewStatus)
  viewStatus?: ViewStatus;

  @IsOptional()
  @IsString()
  @IsEnum(EntityType)
  entityType?: EntityType;
}
