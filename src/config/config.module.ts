import { Module } from '@nestjs/common';
import { AppModule } from './app/app.module';
import { EnvironmentModule } from './environment/environment.module';
import { DatabaseModule } from './database/database.module';

@Module({
  imports: [AppModule, EnvironmentModule, DatabaseModule],
})
export class ConfigModule {}
